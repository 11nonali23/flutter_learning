//Based on ---> https://medium.com/the-web-tub/making-a-todo-app-with-flutter-5c63dab88190

/*
STATELESS = without dynamic changes
STATEFULL = with dynamic changes

STATELESSS widget can have a child statefull one


Why creating two classes for statefull widget?

Since we create a new widget with every change, we cannot store any state in the widget itself, 
as it will be lost with the next change. This is why we need a separate State class.
*/ 

import 'package:flutter/material.dart';

void main() => runApp(new TodoApp());


class TodoApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Todo List',
      home: new TodoList()
    );
  }
}


class TodoList extends StatefulWidget {
  @override
  createState() => new TodoListState();
}


class TodoListState extends State<TodoList> {
  List<String> _todoItems = [];


// Instead of autogenerating a todo item, _addTodoItem now accepts a string
void _addTodoItem(String task) {
  // Only add the task if the user actually entered something
  if(task.length > 0) {
    setState(() => _todoItems.add(task));
  }
}

  // Build the whole list of todo items. 
  /*
  I wanted a separator for my list so I:
    ->Used ListView.separated
    ->Defined a separatorBuilder
    ->Defined itemBuilder as the lenght of list. (Since the widget reaload every time a call state it is ok!)
  */
  Widget _buildTodoList() {
    return new ListView.separated(
      separatorBuilder: (context, index) => Divider(
        color: Colors.black,
      ),
      itemCount: _todoItems.length,
      itemBuilder: (context, index) {
        // itemBuilder will be automatically be called as many times as it takes for the
        // list to fill up its available space, which is most likely more than the
        // number of todo items we have. So, we need to check the index is OK.
        if(index < _todoItems.length) {
          return _buildTodoItem(_todoItems[index], index);
        }
      },
    );
  }

  // Build a single todo item
  Widget _buildTodoItem(String todoText, int index) {
  return new ListTile(
    title: new Text(todoText),
    onTap: () => _promptRemoveTodoItem(index)
  );
}

// Much like _addTodoItem, this modifies the array of todo strings and
// notifies the app that the state has changed by using setState
void _removeTodoItem(int index) {
  setState(() => _todoItems.removeAt(index));
}

// Show an alert dialog asking the user to confirm that the task is done
void _promptRemoveTodoItem(int index) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return new AlertDialog(
        title: new Text('Mark "${_todoItems[index]}" as done?'),
        actions: <Widget>[
          new FlatButton(
            child: new Text('MANTAIN'),
            onPressed: () => Navigator.of(context).pop()
          ),
          new FlatButton(
            child: new Text('MARK AS DONE'),
            onPressed: () {
              _removeTodoItem(index);
              Navigator.of(context).pop();
            }
          )
        ]
      );
    }
  );
}

 Widget build(BuildContext context) {
  return new Scaffold(
    appBar: new AppBar(
      title: new Text('Todo List')
    ),
    body: _buildTodoList(),
    floatingActionButton: new FloatingActionButton(
      onPressed: _pushAddTodoScreen, // pressing this button now opens the new screen
      tooltip: 'Add task',
      child: new Icon(Icons.add)
    ),
  );
}


void _pushAddTodoScreen() {
  // Push this page onto the stack
  Navigator.of(context).push(
    // MaterialPageRoute will automatically animate the screen entry, as well
    // as adding a back button to close it
    _buildToDoText()
  );
}

//I have created a method for the text of ToDo in order to have more clarity
MaterialPageRoute _buildToDoText() {
  return new MaterialPageRoute(
      builder: (context) {
        return new Scaffold(
          appBar: new AppBar(
            title: new Text('Add a new task')
          ),
          body: new TextField(
            autofocus: true,
            onSubmitted: (val) {
              _addTodoItem(val);
              Navigator.pop(context); // Close the add todo screen
            },
            decoration: new InputDecoration(
              hintText: 'Enter something to do...',
              contentPadding: const EdgeInsets.all(16.0)
            ),
          )
        );
      }
    );
}

}
