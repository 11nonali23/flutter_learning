import 'package:flutter/material.dart';
import 'package:like_dislike/homePage.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(title: Text("Login")), body: Body());
  }
}

//stateful così se l' utente torna indietro posso riottenere ciò che ha scritto in precedenza

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  String name;
  TextEditingController controller = new TextEditingController();

  void click() {
    if (controller.text.length != 0) {
      this.name = controller.text;
      Navigator.push(context,
          MaterialPageRoute(builder: (context) => HomePage(this.name)));
    } else {
      //TODO
    }
  }

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Padding(
          padding: EdgeInsets.all(10),
          child: TextField(
            controller: this.controller,
            decoration: InputDecoration(
                prefixIcon: Icon(Icons.person),
                labelText: "Type your name:",
                border: OutlineInputBorder(
                    borderSide: BorderSide(width: 5, color: Colors.black)),
                suffixIcon: IconButton(
                  icon: Icon(Icons.done),
                  splashColor: Colors.blue,
                  tooltip: "Submit",
                  onPressed: () => this.click(),
                )),
          ),
        ));
  }
}
