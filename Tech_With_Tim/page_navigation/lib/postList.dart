import 'package:flutter/material.dart';
import 'post.dart';

class PostList extends StatefulWidget {
  List<Post> itemList;

  PostList(this.itemList);

  @override
  _PostListState createState() => _PostListState();
}

//listview builder definisce un modo di creare la lista. Genera come un elemento della lista si presenta.
//Questo permette tutta l'efficienza del recyclerview di Android
class _PostListState extends State<PostList> {
  //creo un metodo like, perchè voglio che quando chiamo il metodo post.likePost venga
  //aggiornato lo state e la view sarà ridisegnata. Così aggiorno il numero di likes.
  //é utilizzata la tecnica callback descritta nella lezione "button triggering" che permette di chiamare funzioni da classi esterne
  void like(Function callBack) {
    this.setState(() {
      callBack();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.widget.itemList.length,
      itemBuilder: (context, index) {
        var post = this.widget.itemList[index];
        return Card(
          child: Row(
            children: <Widget>[
              Expanded(
                  child: ListTile(
                title: Text(post.body),
                subtitle: Text(post.author),
              )),
              Row(children: <Widget>[
                Container(
                  child: Text(post.likes.toString(),
                      style: TextStyle(fontSize: 17)),
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                ),
                IconButton(
                    icon: Icon(Icons.thumb_up),
                    color: post.userLiked ? Colors.green : Colors.black,
                    splashColor: post.userLiked ? Colors.green : Colors.black,
                    onPressed: () => like(post.likePost)),
              ])
            ],
          ),
        );
      },
    );
  }
}
