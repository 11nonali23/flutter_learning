import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Widget as the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Andreas app',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity:
            VisualDensity.adaptivePlatformDensity, //change feel based on OS
      ),
      home: HomePage(), //home page of app
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("hello world")), body: TextInputWidget());
  }
}

class TextInputWidget extends StatefulWidget {
  @override
  _TextInputWidgetState createState() => _TextInputWidgetState();
}

class _TextInputWidgetState extends State<TextInputWidget> {
  final controller =
      TextEditingController(); //oggetto da attach al textinput controlla cosa c'è nel input field
  String text = "";
  //per richiedere a flutter di ridisegnare l'app devo usare anche il metodo dispose
  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  //setState ridisegna l'app :-)
  void updateText(text) {
    if (text == "h ello") {
      controller.clear();
      text = "";
    }
    setState(() {
      this.text = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      TextField(
        controller: this.controller,
        decoration: InputDecoration(
            prefixIcon: Icon(Icons.message), labelText: "Type a message:"),
        onChanged: (text) => this.updateText(text),
      ),
      Text("the text is: " + this.text)
    ]);
  }
}
