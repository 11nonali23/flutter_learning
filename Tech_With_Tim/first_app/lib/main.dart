import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Widget as the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Andreas app',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity:
            VisualDensity.adaptivePlatformDensity, //change feel based on OS
      ),
      home: HomePage(), //home page of app
    );
  }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("hello world")),
      body: Column(
        //esiste anche row
        children: <Widget>[TestWidget(), TestWidget(), TestWidget()],
      ),
    );
  }
}

class TestWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Text("Hello World!");
  }
}
