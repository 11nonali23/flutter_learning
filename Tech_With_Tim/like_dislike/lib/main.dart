import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class Post {
  String body;
  String author;
  int likes = 0;
  bool userLiked = false;

  Post(this.body, this.author);

  void likePost() {
    this.userLiked = !this.userLiked;
    if (this.userLiked) {
      this.likes += 1;
    } else {
      this.likes -= 1;
    }
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Andreas app',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Post> posts = [];

  void newPost(String text) {
    if (text.length != 0) {
      this.setState(() {
        posts.add(new Post(text, "Andrea"));
      });
    } else {
      //TODO
    }
  }

// Expanded è come match parent
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("hello world")),
        body: Column(children: <Widget>[
          Expanded(child: PostList(this.posts)),
          TextInputWidget(this.newPost)
        ]));
  }
}

class TextInputWidget extends StatefulWidget {
  final Function(String) callback;
  TextInputWidget(this.callback);

  @override
  _TextInputWidgetState createState() => _TextInputWidgetState();
}

class _TextInputWidgetState extends State<TextInputWidget> {
  final controller = TextEditingController();
  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void click() {
    widget.callback(controller.text);
    FocusScope.of(context).unfocus();
    controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: this.controller,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.message),
          labelText: "Type a message:",
          suffixIcon: IconButton(
            icon: Icon(Icons.send),
            splashColor: Colors.blue,
            tooltip: "Post message",
            onPressed: () => this.click(),
          )),
    );
  }
}

class PostList extends StatefulWidget {
  List<Post> itemList;

  PostList(this.itemList);

  @override
  _PostListState createState() => _PostListState();
}

//listview builder definisce un modo di creare la lista. Genera come un elemento della lista si presenta.
//Questo permette tutta l'efficienza del recyclerview di Android
class _PostListState extends State<PostList> {
  //creo un metodo like, perchè voglio che quando chiamo il metodo post.likePost venga
  //aggiornato lo state e la view sarà ridisegnata. Così aggiorno il numero di likes.
  //é utilizzata la tecnica callback descritta nella lezione "button triggering" che permette di chiamare funzioni da classi esterne
  void like(Function callBack) {
    this.setState(() {
      callBack();
    });
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: this.widget.itemList.length,
      itemBuilder: (context, index) {
        var post = this.widget.itemList[index];
        return Card(
          child: Row(
            children: <Widget>[
              Expanded(
                  child: ListTile(
                title: Text(post.body),
                subtitle: Text(post.author),
              )),
              Row(children: <Widget>[
                Container(
                  child: Text(post.likes.toString(),
                      style: TextStyle(fontSize: 17)),
                  padding: EdgeInsets.fromLTRB(0, 0, 3, 0),
                ),
                IconButton(
                    icon: Icon(Icons.thumb_up),
                    color: post.userLiked ? Colors.green : Colors.black,
                    splashColor: post.userLiked ? Colors.green : Colors.black,
                    onPressed: () => like(post.likePost)),
              ])
            ],
          ),
        );
      },
    );
  }
}
