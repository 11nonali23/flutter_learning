import 'package:flutter/material.dart';

//IMPARO: chiamare il metodo di un altra classe col bottone send
/*--------------------------------------------------------------------------------------------------
Breve spiegazione dell' idea:                                                                      

considero 
void test(){} è una funzione

test --> se chiamo la funzione senza () allora ottero il riferimento a dove è locata la funzione

x = test;
x()   ----> in questo passo, siccome x è = al riferimento di test, chiamerò la funzione test().

posso fare lo stesso con le callback: 
passo il riferimento della funzione al costruttore della classe esterna che dovrà chiamarla
/--------------------------------------------------------------------------------------------------*/

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // Widget as the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Andreas app',
      theme: ThemeData(
        primarySwatch: Colors.orange,
        visualDensity:
            VisualDensity.adaptivePlatformDensity, //change feel based on OS
      ),
      home: HomePage(), //home page of app
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String text = "";

  void updateText(String text) {
    this.setState(() {
      this.text = text;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("hello world")),
        body: Column(children: <Widget>[
          TextInputWidget(
              this.updateText), //passo la funzione che è di callback
          Text("text is: " + this.text)
        ]));
  }
}

class TextInputWidget extends StatefulWidget {
  final Function(String) callback; //stessi parametri di updateText

  //costruttore: se mettessi il parametro callback in {} allora sarebbe opzionale
  //se mettessi il parametro callback in [] allora sarebbe unnamed e opzionale
  TextInputWidget(this.callback);

  @override
  _TextInputWidgetState createState() => _TextInputWidgetState();
}

//--------------------------------------------------------------------------------------------------
//callback permetterà di modifcare tramite il bottone di questa classe quello della classe HomePage |
//--------------------------------------------------------------------------------------------------

class _TextInputWidgetState extends State<TextInputWidget> {
  final controller =
      TextEditingController(); //oggetto da attach al textinput controlla cosa c'è nel input field
  //per richiedere a flutter di ridisegnare l'app devo usare anche il metodo dispose
  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }

  void click() {
    widget.callback(controller.text);
    controller.clear();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: this.controller,
      decoration: InputDecoration(
          prefixIcon: Icon(Icons.message),
          labelText: "Type a message:",
          suffixIcon: IconButton(
            icon: Icon(Icons.send),
            splashColor: Colors.blue,
            tooltip: "Post message",
            onPressed: () => this.click(),
          )),
    );
  }
}
